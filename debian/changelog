globus-gsi-openssl-error (4.4-2) unstable; urgency=medium

  * Make doxygen Build-Depends-Indep
  * Drop old debug symbol migration from 2017

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 05 Jul 2022 22:18:15 +0200

globus-gsi-openssl-error (4.4-1) unstable; urgency=medium

  * New GCT release v6.2.20220524
  * Drop patches included in the release

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Wed, 25 May 2022 22:13:19 +0200

globus-gsi-openssl-error (4.3-2) unstable; urgency=medium

  * Function names are (null) in OpenSSL 3.0 - adapt expected test output
    (Closes: #996288)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 15 Oct 2021 16:21:41 +0200

globus-gsi-openssl-error (4.3-1) unstable; urgency=medium

  * OpenSSL 3.0 fixes

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 17 Aug 2021 16:36:12 +0200

globus-gsi-openssl-error (4.2-1) unstable; urgency=medium

  * Minor fixes to makefiles
  * Change to debhelper compat level 13
  * Remove override_dh_missing rule (--fail-missing is default)
  * Drop old symlink-to-dir conversion from 2014

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 15 Dec 2020 10:45:07 +0100

globus-gsi-openssl-error (4.1-2) unstable; urgency=medium

  * Convert debian/rules to dh tool
  * Change to debhelper compat level 10
  * Update documentation links in README file

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 12 Jul 2019 16:34:43 +0200

globus-gsi-openssl-error (4.1-1) unstable; urgency=medium

  * Doxygen fixes
  * Use .maintscript file for dpkg-maintscript-helper

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Wed, 27 Feb 2019 20:00:55 +0100

globus-gsi-openssl-error (4.0-1) unstable; urgency=medium

  * Switch upstream to Grid Community Toolkit
  * First Grid Community Toolkit release
  * Move VCS to salsa.debian.org

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sun, 16 Sep 2018 03:16:31 +0200

globus-gsi-openssl-error (3.8-2) unstable; urgency=medium

  * Migrate to dbgsym packages
  * Support DEB_BUILD_OPTIONS=nocheck

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 06 Jul 2017 12:58:34 +0200

globus-gsi-openssl-error (3.8-1) unstable; urgency=medium

  * GT6 update: Alter dependency order to avoid mixing SSL versions

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 12 Jan 2017 14:30:31 +0100

globus-gsi-openssl-error (3.7-1) unstable; urgency=medium

  * GT6 update

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 08 Nov 2016 21:46:11 +0100

globus-gsi-openssl-error (3.6-1) unstable; urgency=medium

  * GT6 update: Updates for OpenSSL 1.1.0 (Closes: #828319)
  * Change Maintainer e-mail (fysast → physics)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 01 Sep 2016 09:06:44 +0200

globus-gsi-openssl-error (3.5-5) unstable; urgency=medium

  * Set SOURCE_DATE_EPOCH and rebuild using doxygen 1.8.11
    (for reproducible build)
  * Update URLs (use toolkit.globus.org)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 16 Feb 2016 15:41:49 +0100

globus-gsi-openssl-error (3.5-4) unstable; urgency=medium

  * Rebuild using doxygen 1.8.9.1 (Closes: #630059)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 30 Apr 2015 12:54:59 +0200

globus-gsi-openssl-error (3.5-3) unstable; urgency=medium

  * Add Pre-Depends for dpkg-maintscript-helper

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 08 Nov 2014 23:12:27 +0100

globus-gsi-openssl-error (3.5-2) unstable; urgency=medium

  * Properly handle symlink-to-dir conversion in doc package
  * Enable verbose tests

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 07 Nov 2014 16:49:19 +0100

globus-gsi-openssl-error (3.5-1) unstable; urgency=medium

  * GT6 update
  * Drop patch globus-gsi-openssl-error-doxygen.patch (fixed upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Mon, 27 Oct 2014 09:44:11 +0100

globus-gsi-openssl-error (3.4-1) unstable; urgency=medium

  * Update to Globus Toolkit 6.0
  * Drop GPT build system and GPT packaging metadata
  * Enable checks

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 23 Sep 2014 09:22:10 +0200

globus-gsi-openssl-error (2.1-5) unstable; urgency=low

  * Remove junk man page

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 05 Dec 2013 21:40:40 +0100

globus-gsi-openssl-error (2.1-4) unstable; urgency=low

  * Implement Multi-Arch support
  * Rename dbg package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 09 Nov 2013 15:43:47 +0100

globus-gsi-openssl-error (2.1-3) unstable; urgency=low

  * Add arm64 to the list of 64 bit architectures

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 26 May 2013 08:27:34 +0200

globus-gsi-openssl-error (2.1-2) unstable; urgency=low

  * Fix broken links in README file

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 24 Jan 2012 15:55:28 +0100

globus-gsi-openssl-error (2.1-1) unstable; urgency=low

  * Update to Globus Toolkit version 5.2.0
  * Make doc package architecture independent

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 23 Dec 2011 18:35:43 +0100

globus-gsi-openssl-error (0.14-8) unstable; urgency=low

  * Use system jquery script

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 07 Jun 2011 01:02:43 +0200

globus-gsi-openssl-error (0.14-7) unstable; urgency=low

  * Add README file
  * Use new doxygen-latex build dependency (Closes: #616237)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Mon, 25 Apr 2011 23:47:11 +0200

globus-gsi-openssl-error (0.14-6) unstable; urgency=low

  * Converting to package format 3.0 (quilt)
  * Add new build dependency on texlive-font-utils due to changes in texlive
    packaging (epstopdf moved there) (Closes: #583054)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 04 Jun 2010 04:43:30 +0200

globus-gsi-openssl-error (0.14-5) unstable; urgency=low

  * Update to Globus Toolkit 5.0.0
  * Add debug package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 26 Jan 2010 11:28:57 +0100

globus-gsi-openssl-error (0.14-4) unstable; urgency=low

  * Fix rule dependencies in the debian/rules file.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 13 May 2009 11:02:48 +0200

globus-gsi-openssl-error (0.14-3) unstable; urgency=low

  * Initial release (Closes: #521850).
  * Rebuilt to correct libltdl dependency.
  * Preparing for other 64bit platforms than amd64.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 18 Apr 2009 20:17:32 +0200

globus-gsi-openssl-error (0.14-2) UNRELEASED; urgency=low

  * Only quote the Apache-2.0 license if necessary.
  * Updated deprecated Source-Version in debian/control.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 26 Mar 2009 09:21:25 +0100

globus-gsi-openssl-error (0.14-1) UNRELEASED; urgency=low

  * First build.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 02 Jan 2009 08:46:55 +0100
